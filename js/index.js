
const req = async function(url) {
    document.querySelector(".box-loader").classList.add("show");
    const data = await fetch(url);
    return data.json();
};

// Получаем комментарии с jasonplaceholder

const dataComments = document.querySelector(".comments");

dataComments.addEventListener("click", (e) => {
    req("https://jsonplaceholder.typicode.com/comments")
    .then((info) => {
        showData(info);
    })
    .catch((error) => {
        console.error(error)
    })
});

// Получаем список дел с jasonplaceholder

const dataTodos = document.querySelector(".todos");

dataTodos.addEventListener("click", (e) => {
    req("https://jsonplaceholder.typicode.com/todos")
    .then((info) => {
        showData(info);
    })
    .catch((error) => {
        console.error(error)
    })
});


// Вывод данных в таблицу


function showData(data) {

    const tbody = document.querySelector("tbody");

    tbody.innerHTML = "";

    const myArray = data.map((element, i, title, completed) => {
        return {
            id : i + 1,
            name : element.name || element.title,
            body : element.body || element.completed
        }
    })

    myArray.forEach(element => {
        tbody.insertAdjacentHTML("beforeend", `
        <tr>
            <td>${element.id}</td>
            <td>${element.name}</td>
            <td>${element.body}</td>
        </tr>`)
    });

    document.querySelector(".box-loader").classList.remove("show");
}
    






